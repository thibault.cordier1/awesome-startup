import React from 'react';

import Amplify from 'aws-amplify';
import awsconfig from '../aws-exports';
import { BrowserRouter, Route, Switch } from "react-router-dom";
import Home from './pages/home';
import HomeAdmin from './pages/home-admin';
import RegisterPage from './pages/users/register';
import SignInPage from './pages/users/signin';
import { UserProvider } from './contexts/user';
import { LocaleContextProvider } from './contexts/locale';
import SignoutForm from './components/SignOutForm';
import AppNavbar from './components/Navbar';



Amplify.configure(awsconfig);



function App(props) {

  return (
    <BrowserRouter>
      <UserProvider>
        <LocaleContextProvider>
          <div className="App">
            <div className="container-fluid">
              <AppNavbar />
              <div className="row">
                <div className="text-left col-4">
                  <SignoutForm />
                </div>
              </div>

              <div className="container mt-4">
                <Switch>
                  <Route exact path="/"><Home /></Route>
                  <Route exact path="/users/register"><RegisterPage /></Route>
                  <Route exact path="/users/signin"><SignInPage /></Route>
                  <Route component={HomeAdmin} />
                </Switch>
              </div>
            </div>
          </div>
        </LocaleContextProvider>
      </UserProvider>
    </BrowserRouter>
  );
}

export default App;


