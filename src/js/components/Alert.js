const { I18n } = require("aws-amplify");

function Alert(props) {
    if(props.message && props.message.length > 0) {
        return <div className={props.className} role="alert">
        {I18n.get(props.message)}
    </div>
    } else {
        return ''
    }
}

export default Alert;