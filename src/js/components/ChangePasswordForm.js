import { I18n } from "aws-amplify";
import { useState } from "react";
import { Lock, Person, PersonBadge } from "react-bootstrap-icons";
import { useUser } from "../contexts/user";
import { toSnakeCase } from "../utils/strings";

const { useForm } = require("react-hook-form");

function ChangePasswordForm(props) {
    const { register, errors, handleSubmit } = useForm();
    const [changePasswordButtonIsDisabled, setButtonStatus] = useState(false);
    const [successMessage] = useState(null);
    const [errorMessage, setErrorMessage] = useState(null);

    const { changePassword } = useUser();

    const onSubmit = (values) => {
        setButtonStatus(true);
        changePassword(props.username, values.code_forgot_password, values.password_forgot_password)
        .then((response) => {

            console.log(response);
            setButtonStatus(false);
        })
        .catch((error) => {
            setErrorMessage(toSnakeCase(error.name))
            setButtonStatus(false);
        })

    }

    let success = '';
    if (successMessage) {
        let successText = I18n.get('code-send-by') + ' ' + I18n.get(successMessage.medium) + ' ' + I18n.get('code-send-to') + ' ' + successMessage.destination;
        success = <div className="alert alert-success" role="alert">
            {successText}
        </div>
    }

    let error = ''

    if (errorMessage) {
        let errorText = I18n.get(errorMessage);
        error = <div className="alert alert-danger" role="alert">
            {errorText}
        </div>
    }


    return <form action="#" onSubmit={handleSubmit(onSubmit)}>
        {success} { error }
        <div className="form-group mb-3">
            <label htmlFor="username">{I18n.get("form-label-username")} *</label>
            <div className="input-group input-group-merge">
                <input className={('username_forgot_password' in errors) ? "form-control is-invalid" : "form-control"} disabled={true} value={props.username} autoComplete="username_forgot_password" name="username_forgot_password" type="text" id="username_forgot_password" required="" placeholder={I18n.get("form-placeholder-username")} />

                <div className="input-group-append" data-password="false">
                    <div className="input-group-text">
                        <Person />
                    </div>
                </div>
                {'username_forgot_password' in errors &&
                    <div className="invalid-feedback">
                        {I18n.get("form-username-invalid")}
                    </div>
                }
            </div>
        </div>
        
        <div className="form-group mb-3">
            <label htmlFor="password">{I18n.get("form-label-code")} *</label>
            <div className="input-group input-group-merge">
                <input id='code_forgot_password' name='code_forgot_password' type="number" className={('code_forgot_password' in errors) ? "form-control is-invalid" : "form-control"} placeholder={I18n.get('form-placeholder-verification-code')} ref={register({ required: true, minLength: 6 })} />
                <div className="input-group-append" data-password="false">
                    <div className="input-group-text">
                        <PersonBadge />
                    </div>
                </div>
                {'code_forgot_password' in errors &&
                    <div className="invalid-feedback">
                        {I18n.get("form-code-invalid")}
                    </div>
                }
            </div>
        </div>


        <div className="form-group mb-3">
                <label htmlFor="password">{I18n.get("form-label-new-password")} *</label>
                <div className="input-group input-group-merge">
                    <input type="password" name="password_forgot_password" autoComplete="new-password" id="password_forgot_password" className={('password_forgot_password' in errors) ? "form-control is-invalid" : "form-control"} required="" placeholder={I18n.get("form-placeholder-password")} ref={register({ required: true, minLength: 6 })} />
                    <div className="input-group-append" data-password="false">
                        <div className="input-group-text">
                            <Lock />
                        </div>
                    </div>
                    {'password_forgot_password' in errors &&
                        <div className="invalid-feedback">
                            {I18n.get("form-password-invalid")}
                        </div>
                    }
                </div>
            </div>

        <div className="form-group mb-0 text-center">
            <button className="btn btn-primary btn-block" type="submit" disabled={changePasswordButtonIsDisabled}>{I18n.get("form-submit-button-password-change")}</button>
        </div>
    </form>

}

export default ChangePasswordForm;