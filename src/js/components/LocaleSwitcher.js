import { useLocale } from "../hooks/useLocale";


function LocaleSwitcher(props) {
    let { locale, changeLanguage } = useLocale();

    const langDict = {
        'en': 'English',
        'fr': 'Français'
    }

    const handleChange = event => {
        changeLanguage(event.target.value)
    }

    return <div className="ml-2">
        <select className="form-control form-control-sm" id="localeSelector" onChange={handleChange} value={locale}>
            {Object.keys(langDict).map((lang, index) => {
                return <option key={index} value={lang}>{langDict[lang]}</option>
            })}
        </select>
    </div>

}

export default LocaleSwitcher;