import { I18n } from "aws-amplify";
import { Button, Nav, Navbar, NavDropdown } from "react-bootstrap";
import { PersonFill, PersonPlusFill, Star } from "react-bootstrap-icons";
import { useHistory } from "react-router-dom";
import { useUser } from "../contexts/user";
import { useLocale } from "../hooks/useLocale";
import LocaleSwitcher from "./LocaleSwitcher";


function AppNavbar(props) {

    useLocale();
    const history = useHistory();
    const handleSelect = (eventKey) => {
        history.push(eventKey);
    };
    const { user } = useUser();


    return <Navbar bg="light" expand="lg" onSelect={handleSelect}>
        <Navbar.Brand eventkey="/" href="#"><h2><span className="text-info p-2"><Star/></span> Foundation</h2></Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="mr-auto">
            </Nav>
            
            {user &&
                <NavDropdown title="Dropdown" id="basic-nav-dropdown">
                    <NavDropdown.Item href="#action/3.1"></NavDropdown.Item>
                    <NavDropdown.Item href="#action/3.2">Another action</NavDropdown.Item>
                    <NavDropdown.Item href="#action/3.3">Something</NavDropdown.Item>
                    <NavDropdown.Divider />
                    <NavDropdown.Item >{I18n.get('signout-button')}</NavDropdown.Item>
                </NavDropdown>
            }
            {!user &&
                <Nav>
                    <Nav.Item>
                        <Nav.Link eventKey="/users/signin" href="#">
                            <Button size={"sm"} variant="info"><PersonFill/> {I18n.get('signin-button')}</Button>
                        </Nav.Link>
                    </Nav.Item>
                    <Nav.Item>
                        <Nav.Link eventKey="/users/register" href="#">
                            <Button size={"sm"} variant="dark"><PersonPlusFill/> {I18n.get('register-button')}</Button>
                        </Nav.Link>
                    </Nav.Item>

                </Nav>
            }
            <LocaleSwitcher/>
        </Navbar.Collapse>

            
    </Navbar>
}

export default AppNavbar;