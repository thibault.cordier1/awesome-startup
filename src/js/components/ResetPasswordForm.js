import { useState } from "react";
import { Person } from "react-bootstrap-icons";
import { useForm } from "react-hook-form";
import { useUser } from "../contexts/user";
import { toSnakeCase } from "../utils/strings";

const { I18n } = require("aws-amplify");

function ResetPasswordForm(props) {

    const { handleSubmit, register, errors } = useForm();
    const { resetPassword } = useUser();
    const [successMessage, setSuccessMessage] = useState(null);
    const [errorMessage, setErrorMessage] = useState(null);
    const [SendCodeButtonIsDisabled, setButtonStatus] = useState(false);

    let success = '';


    if (successMessage) {
        let successText = I18n.get('code-send-by') + ' ' + I18n.get(successMessage.medium) + ' ' + I18n.get('code-send-to') + ' ' + successMessage.destination;
        success = <div className="alert alert-success" role="alert">
            {successText}
        </div>
    }

    let error = ''

    if (errorMessage) {
        let errorText = I18n.get(errorMessage);
        error = <div className="alert alert-danger" role="alert">
            {errorText}
        </div>
    }




    const onSubmit = (values) => {
        setButtonStatus(true);
        setSuccessMessage(null);
        setErrorMessage(null);
        resetPassword(values.username_reset)
            .then((response) => {
                setButtonStatus(true);
                setSuccessMessage({
                    'medium': response.CodeDeliveryDetails['DeliveryMedium'],
                    'destination': response.CodeDeliveryDetails['Destination']
                })
            })
            .catch((error) => {
                setErrorMessage(toSnakeCase(error.name))
                setButtonStatus(false);
            });
    }




    return <div>
        <form action="#" onSubmit={handleSubmit(onSubmit)}>
            <span>{success}</span> {error}
            <div className="form-group mb-3">
                <label htmlFor="username-reset">{I18n.get("form-label-username")} *</label>
                <div className="input-group input-group-merge">
                    <input className={('username_reset' in errors) ? "form-control is-invalid" : "form-control"} name="username_reset" type="text" id="username_reset" required="" ref={register({ required: true, minLength: 2 })} placeholder={I18n.get("form-placeholder-username")} />
                    <div className="input-group-append" data-password="false">
                        <div className="input-group-text">
                            <Person />
                        </div>
                    </div>
                    {'username_reset' in errors &&
                        <div className="invalid-feedback">
                            {I18n.get("form-username-invalid")}
                        </div>
                    }

                </div>
            </div>
            <div className="form-group mb-0 text-center">
                <button className="btn btn-primary btn-block" type="submit" disabled={SendCodeButtonIsDisabled}>{I18n.get("form-button-send-code")}</button>
            </div>

        </form>
    </div>
}

export default ResetPasswordForm;