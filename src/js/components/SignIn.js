

import { I18n } from 'aws-amplify';
import { useEffect, useState } from 'react';
import { Lock, Person } from 'react-bootstrap-icons';
import { useForm } from "react-hook-form";
import { useUser } from '../contexts/user';
import Spinner from './Spinner';

function SignIn(props) {
    const { handleSubmit, register, errors } = useForm();
    const { state, signIn } = useUser();
    const [SigninButtonIsDisabled, setButtonStatus] = useState(false);

    const onSubmit = (values, e) => {
        setButtonStatus(true);
        signIn(values);
    }

    useEffect(() => {
        if (state && (state.trying === true || state.user)) {
            setButtonStatus(true)
        } else {
            setButtonStatus(false)
        }

    }, [state, errors])

    let alert = '';
    if (state && state.error) {
        alert = <div className="alert alert-warning" role="alert">
            {I18n.get(state.error)}
        </div>
    }



    let tryingSpinner = <Spinner spin={state && state.trying} />


    return <div>
        <form onSubmit={handleSubmit(onSubmit)}>
            {alert}
            <div className="form-group mb-3">
                <label htmlFor="username">{I18n.get("form-label-username")} *</label>
                <div className="input-group input-group-merge">
                    <input className={('username' in errors) ? "form-control is-invalid" : "form-control"} autoComplete="username" name="username" type="text" id="username-signin" required="" placeholder={I18n.get("form-placeholder-username")} ref={register({ required: true, minLength: 2 })} />

                    <div className="input-group-append" data-password="false">
                        <div className="input-group-text">
                            <Person />
                        </div>
                    </div>
                    {'username' in errors &&
                        <div className="invalid-feedback">
                            {I18n.get("form-username-invalid")}
                        </div>
                    }
                </div>
            </div>

            <div className="form-group mb-3">
                <label htmlFor="password">{I18n.get("form-label-password")} *</label>
                <div className="input-group input-group-merge">
                    <input type="password" name="password" autoComplete="current-password" id="password-signin" className={('password' in errors) ? "form-control is-invalid" : "form-control"} required="" placeholder={I18n.get("form-placeholder-password")} ref={register({ required: true, minLength: 6 })} />
                    <div className="input-group-append" data-password="false">
                        <div className="input-group-text">
                            <Lock />
                        </div>
                    </div>
                    {'password' in errors &&
                        <div className="invalid-feedback">
                            {I18n.get("form-password-invalid")}
                        </div>
                    }
                </div>
            </div>


            <div className="form-group mb-0 text-center">
                <button className="btn btn-primary btn-block" type="submit" disabled={SigninButtonIsDisabled}>{I18n.get("form-submit-button-signin")} {tryingSpinner}</button>
            </div>

        </form>
    </div>
}

export default SignIn