import { useUser } from '../contexts/user';

const { I18n } = require("aws-amplify");

function SignoutForm(props) {

    const {state, signOut} = useUser();


    if (state && state.user) {
        return <div>
            <button className="btn btn-primary" type="button" onClick={signOut}>{I18n.get("form-button-signout")}</button>
        </div>
    } else {
        return ''
    }
}

export default SignoutForm;