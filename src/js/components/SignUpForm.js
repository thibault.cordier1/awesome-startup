import { useEffect, useState } from "react";
import { Lock, Person, Envelope } from "react-bootstrap-icons";
import { useForm } from "react-hook-form";
import { useUser } from "../contexts/user";

const { I18n } = require("aws-amplify");

function SignUpForm(props) {
    const { signUp } = useUser();
    const { handleSubmit, register, errors } = useForm();
    const [SignUpButtonIsDisabled, setButtonStatus] = useState(false);
    const [phase, setPhase] = useState('first');

    useEffect(() => {

    }, [phase])


    const onSubmit = (values) => {
        setButtonStatus(true);
        console.log(values);
        signUp(values).then((response) => {
            console.log(response);
            setPhase('code');
        }).catch((error) => {
            console.log(error);
            setButtonStatus(false);
        });
    }

    return <form onSubmit={handleSubmit(onSubmit)}>
        <div className="form-group mb-3">
            <label htmlFor="email-signup">{I18n.get("form-label-email")} *</label>
            <div className="input-group input-group-merge">
                <input className={('email' in errors) ? "form-control is-invalid" : "form-control"} type="text" autoComplete="email" name="email" id="email-signup" required="" ref={register({ required: true, pattern: { value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i, message: I18n.get('invalid-email') }, })} placeholder={I18n.get("form-placeholder-email")} />
                <div className="input-group-append" data-password="false">
                    <div className="input-group-text">
                        <Envelope />
                    </div>
                </div>
                {'email' in errors &&
                    <div className="invalid-feedback">
                        {I18n.get("form-email-invalid")}
                    </div>
                }
            </div>
        </div>

        <div className="form-group mb-3">
            <label htmlFor="username">{I18n.get("form-label-username")} *</label>
            <div className="input-group input-group-merge">
                <input className={('username' in errors) ? "form-control is-invalid" : "form-control"} type="text" name="username" autoComplete="username" id="username-signup" required="" ref={register({ required: true, minLength: 2 })} placeholder={I18n.get("form-placeholder-username")} />
                <div className="input-group-append" data-password="false">
                    <div className="input-group-text">
                        <Person />
                    </div>
                </div>
                {'username' in errors &&
                    <div className="invalid-feedback">
                        {I18n.get("form-username-invalid")}
                    </div>
                }
            </div>
        </div>

        <div className="form-group mb-3">
            <label htmlFor="password">{I18n.get("form-label-password")} *</label>
            <div className="input-group input-group-merge">
                <input type="password" id="password" autoComplete="new-password" name="password" className={('password' in errors) ? "form-control is-invalid" : "form-control"} ref={register({ required: true, minLength: 8 })} placeholder={I18n.get("form-placeholder-password")} />
                <div className="input-group-append" data-password="false">
                    <div className="input-group-text">
                        <Lock />
                    </div>
                </div>
                {'password' in errors &&
                    <div className="invalid-feedback">
                        {I18n.get("form-password-invalid")}
                    </div>
                }
            </div>
        </div>



        <div className="form-group mb-0 text-center">
            <button className="btn btn-primary btn-block" type="submit" disabled={SignUpButtonIsDisabled}>{I18n.get("form-submit-button-signup")}</button>
        </div>

    </form>


}

export default SignUpForm;