import { I18n } from "aws-amplify";
import { useState } from "react";
import { PersonBadge, Person } from "react-bootstrap-icons";
import { useForm } from "react-hook-form";
import { useUser } from "../contexts/user";
import { toSnakeCase } from "../utils/strings";
import Alert from "./Alert";
import Spinner from "./Spinner";


function SignVerifyCode(props) {
    const { handleSubmit, register, errors, reset } = useForm();
    const { state, verifyCode, resendSignUpCode } = useUser();
    const [error, setError] =  useState(null);
    const [success, setSuccess] = useState(null);
    const [SigninButtonIsDisabled, setButtonStatus] = useState(false);
    let tryingSpinner = <Spinner spin={state && state.trying} />

    const onSubmit = (values) => {
        console.log(values);
        setButtonStatus(true);
        try {
            verifyCode(props.userName, values.code)
            .then((response) => {
                console.log(response);
                setError(null);
                setButtonStatus(false);
                reset()
            }).catch((error) => {
                setError(toSnakeCase(error.code))
                setButtonStatus(false);
                reset()
            });
        } catch (error) {
            setError('Unknown error')
                setButtonStatus(false);
                reset()
        }
    }

    const resendCode = () => {
        setButtonStatus(true);
        try {
            resendSignUpCode(props.userName)
            .then((response) => {
                console.log(response);
                setSuccess(response.destination);
                setButtonStatus(false);
                setError(null);
                reset();
            }).catch((error) => {
                setError(toSnakeCase(error.code))
                setButtonStatus(false);
                reset()
            })
        } catch (error) {
            console.log(error);
            setError('Unknown error')
                setButtonStatus(false);
                reset()
        }
    }



    return <div>
        <Alert message={error} className={"alert alert-warning"}  />
        <Alert message={success} className={"alert alert-success"}  />
        <form action="#" onSubmit={handleSubmit(onSubmit)}>
            <div className="form-group mb-3">
                <label htmlFor="username">{I18n.get("form-label-username")} *</label>
                <div className="input-group input-group-merge">
                    <input disabled={true} className={('username' in errors) ? "form-control is-invalid" : "form-control"} autoComplete="username" value={props.userName} name="username" type="text" id="username-code-verification" required="" placeholder={I18n.get("form-placeholder-username")} />

                    <div className="input-group-append" data-password="false">
                        <div className="input-group-text">
                            <Person />
                        </div>

                    </div>
                    {'username' in errors &&
                        <div className="invalid-feedback">
                            {I18n.get("form-username-invalid")}
                        </div>
                    }
                </div>
            </div>
            <div className="form-group mb-3">
                <label htmlFor="password">{I18n.get("form-label-code")} *</label>
                <div className="input-group input-group-merge">
                    <input id='code' name='code' type="number" className={('code' in errors) ? "form-control is-invalid" : "form-control"} placeholder={I18n.get('form-placeholder-verification-code')} ref={register({ required: true, minLength: 6 })} />
                    <div className="input-group-append" data-password="false">
                        <div className="input-group-text">
                            <PersonBadge />
                        </div>
                    </div>
                    {'code' in errors &&
                        <div className="invalid-feedback">
                            {I18n.get("form-code-invalid")}
                        </div>
                    }
                </div>
            </div>
            <div className="form-group mb-0 text-center">
                <button className="btn btn-primary btn-block" type="submit" disabled={SigninButtonIsDisabled}>{I18n.get("form-submit-button-verification-code")} {tryingSpinner}</button>
            </div>
        </form>

        <p><button className="btn btn-link" onClick={resendCode}><small>{I18n.get("lost-your-code-question")} {I18n.get("resend-code")}</small></button></p>

        
    </div>

}

export default SignVerifyCode;