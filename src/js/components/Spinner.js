import { I18n } from "aws-amplify";

function Spinner(props){
    if(props.spin) {
        return <div className="spinner-grow" role="status">
        <span className="sr-only">{I18n.get("loading")}</span>
    </div>
    } else {
        return ''
    }
}

export default Spinner;