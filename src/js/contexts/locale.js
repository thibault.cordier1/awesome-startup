import { I18n } from "aws-amplify";
import { createContext, useReducer } from "react";
import appI18nDict from '../i18n/app';

const initialState = {
  locale: 'en'
};

if(navigator.language.startsWith('fr')) {
  initialState.locale = 'fr'
} 

I18n.putVocabularies(appI18nDict)

// Context
export const LocaleContext = new createContext();

// Reducer
function localeReducer(state, action) {
  switch (action.type) {
      case 'CHANGE_LOCALE':
          I18n.setLanguage(action.locale);
          return {locale: action.locale};
      default:
          return state;
  }
}
// Provider (to wrap components)
export const LocaleContextProvider = (props) => {
  const [localeState, localeDispatch] = useReducer(localeReducer, initialState);
  let value = { localeState, localeDispatch };
  return (
    <LocaleContext.Provider value={value}>
      {props.children}
    </LocaleContext.Provider>
  );
};
