import { Auth, Hub } from "aws-amplify";
import { createContext, useContext, useEffect, useReducer } from "react";
import { useHistory } from "react-router-dom";
import { toSnakeCase } from "../utils/strings";


const UserContext = new createContext();


const init = () => {
    return {
        user: null,
        error: null,
        trying: false
    }
}


// Reducer
function userReducer(state, action) {
    switch (action.type) {
        case 'signedIn':
            console.log('User: ' + action.user.username + ' has signed-in');
            return {
                user: action.user
            };
        case 'startSignIn':
            return {
                trying: true
            }
        case 'stopSignInWithError':
            return {
                trying: false,
                error: action.error
            }
        case 'signIn':
            return state;
        case 'signOut':
            return {
                user: null,
                trying: false,
                error: false
            }
        default:
            throw new Error(`Unhandled action type: ${action.type}`)
    }
}

// Provider (to wrap components)
function UserProvider({ children }) {
    const [state, dispatch] = useReducer(userReducer, {}, init);
    return (
        <UserContext.Provider value={{ state, dispatch }}>
            {children}
        </UserContext.Provider>
    );
};



function useUser() {
    const { state, dispatch } = useContext(UserContext)
    if (state === undefined) {
        throw new Error('useUser must be used within a UserProvider')
    }
    const history = useHistory();
    useEffect(() => {

        Auth.currentAuthenticatedUser()
            .then(user => dispatch({ type: 'signedIn', user: user }))
            .catch(() => '');
        Hub.listen('auth', data => {
            switch (data.payload.event) {
                case 'signIn':
                    return dispatch({ type: 'signedIn', user: data.payload.data });
                case 'signIn_failure':
                    console.log('siginIn_failure');
                    return '';
                case 'signOut':
                    history.push('/');
                    return ''
                default:

            }
        });

    }, [dispatch, history])

    const signOut = () => {
        Auth.signOut().then(() => {
            console.log('Logout Done')
        }).catch(() => {
            console.log('Logout error')
        }).then(() => {
            dispatch({ type: 'signOut' })
        });
    }


    const signIn = (payload) => {
        try {
            dispatch({ type: 'startSignIn' });
            Auth.signIn(payload.username, payload.password)
                .then((response) => {
                    return {
                        error: null,
                        trying: false,
                        user: response
                    }
                })
                .catch((error) => {
                    console.log(error.code);
                    dispatch({ type: 'stopSignInWithError', error: toSnakeCase(error.code) })
                });

        } catch (error) {
            console.log('error signin in', error)
            dispatch({ type: 'stopSignInWithError', error: error.code })
        }
    }

    const signUp = (payload) => {
        return Auth.signUp({
            username: payload.username,
            password: payload.password,
            attributes: {
                email: payload.email
            }
        }
        )
    }

    const resendSignUpCode = (username) => {
        return Auth.resendSignUp(username);
    }

    const resetPassword = (username) => {
        return Auth.forgotPassword(username)
    }

    const changePassword = (username, code, password) => {
        return Auth.forgotPasswordSubmit(username, code, password);
    }

    const verifyCode = (username, code) => {
        return Auth.confirmSignUp(username, code);
    }

    return { state, dispatch, signIn, signOut, signUp, verifyCode, resendSignUpCode, resetPassword, changePassword };
}


export { useUser, UserProvider }