import { useContext } from "react";
import { LocaleContext } from "../contexts/locale";



const useLocale = () => {
    const { localeState, localeDispatch } = useContext(LocaleContext);

    function changeLanguage(lang) {
        localeDispatch({ type: 'CHANGE_LOCALE', locale: lang })
    }

    return {
        changeLanguage,
        locale: localeState.locale
    }
}

export {useLocale};