const appI18nDict = {
    'fr': {
        'welcome-title': 'Bienvenue',
        'form-label-username': "Nom d'utilisateur",
        'form-label-password': "Mot de passe",
        'form-label-new-password': "Nouveau mot de passe",
        'form-label-email': "Email",
        'form-label-code': "Code",
        'form-placeholder-username': "Entrez votre nom d'utilisateur",
        'form-placeholder-password': "Entrez votre mot de passe",
        'form-placeholder-email': "Entrez votre email",
        'form-placeholder-verification-code': "Entrez votre code de vérification",
        'form-label-rememberme': "Se souvenir de moi",
        'form-submit-button-signin': "Se connecter",
        'signin-title': "Se connecter",
        'reset-password-title': "Ré-initaliser votre mot de passe",
        'form-button-send-code': "Envoyer le code",
        'form-button-signout': "Se déconnecter",
        'signup-title': "Créer un nouveau compte",
        'confirm-account-title': 'Confirmer votre compte',
        'form-submit-button-signup': "Créer votre compte",
        'form-username-invalid': "Le nom d'utilisateur est invalide",
        'form-password-invalid': "Le mot de passe est trop court (8 caractères minimum)",
        'form-email-invalid': "L'email est invalide",
        'form-submit-button-verification-code': 'Confirmer',
        'form-submit-button-password-change': "Sauvegarder votre nouveau mot de passe",
        'user-not-found-exception': 'Utilisateur non reconnu',
        'not-authorized-exception': 'Mot de passe incorrect',
        'user-not-confirmed-exception': "Utilisateur non confirmé",
        'code-mismatch-exception': 'Le code est erronné',
        'limit-exceeded-exception': "Nombre d'essai limite atteint, veuillez ré-essayer plus tard",
        'form-code-invalid': 'Le code est trop court (6 min)',
        'lost-your-code-question': "Vous n'avez pas reçu votre code ?",
        'resend-code': 'Ré-envoyer le code',
        'EMAIL': "email",
        'code-send-by': 'Code envoyé par',
        'code-send-to': 'à',
        'signin-button': "Se connecter",
        'register-button': "Créer un compte"
    },
    'en': {
        'welcome-title': "Welcome",
        'form-label-username': "Username",
        'form-label-password': "Password",
        'form-label-new-password': "New password",
        'form-label-email': "Email",
        'form-label-code': "Code",
        'form-placeholder-username': "Enter your username",
        'form-placeholder-password': "Enter your password",
        'form-placeholder-email': "Enter your email",
        'form-placeholder-verification-code': "Enter your code",
        'form-label-rememberme': "Remember me",
        'form-submit-button-signin': "Sign In",
        'form-submit-button-password-change': "Save your new password",
        'signin-title': "Sign In",
        'reset-password-title': "Reset your password",
        'form-button-send-code': "Send code",
        'form-button-signout': "Sign out",
        'signup-title': "Create a new account",
        'confirm-account-title': 'Confirm your account',
        'form-submit-button-signup': "Create account",
        'form-username-invalid': "The username is not valid",
        'form-password-invalid': "The password is too short (minimum 8)",
        'form-email-invalid': "Email is not valid",
        'form-submit-button-verification-code': 'Confirm',
        'user-not-found-exception': 'User not found',
        'not-authorized-exception': 'Incorrect Password',
        'user-not-confirmed-exception': "User not confirmed",
        'code-mismatch-exception': "Code mismatch",
        'form-code-invalid': 'Code is too short (6 min)',
        'lost-your-code-question': "Lost your code ?",
        'resend-code': 'Resend code',
        'EMAIL': 'email',
        'limit-exceeded-exception': 'Attempt limit exceeded, please try after some time.',
        'code-send-by': 'Code send by',
        'code-send-to': 'to',
        'signin-button': "Sign In",
        'register-button': "Sign Up"
    }
};

export default appI18nDict