import { AmplifyAuthenticator, AmplifySignUp } from '@aws-amplify/ui-react';

import SignIn from '../components/SignIn';
import ResetPasswordForm from '../components/ResetPasswordForm'
import { useLocale } from '../hooks/useLocale';
import { I18n } from 'aws-amplify';
import SignUpForm from '../components/SignUpForm';
import SignVerifyCode from '../components/SignVerifyCode';
import ChangePasswordForm from '../components/ChangePasswordForm';



function HomeAdmin(props) {
    useLocale();
    return (
        <div>
            <h2>Home Admin</h2>
            <div className="row">
                <div className="col-5 text-center">

                    <p className="text-muted mb-4 mt-3">{I18n.get('confirm-account-title')}</p>
                    <div className="text-left">
                        <SignVerifyCode userName={"thibault2"} />
                    </div>

                </div>
                <div className="col-5 text-center">
                    <div className="text-center">
                        <p className="text-muted mb-4 mt-3">{I18n.get('reset-password-title')}</p>
                    </div>
                    <div className="text-left">
                        <ResetPasswordForm />

                    </div>

                    
                    <div className="text-left">
                        <SignUpForm />
                    </div>

                </div>
                <div className="col-5 text-center">
                <ChangePasswordForm username={"thibault2"}/>

                </div>

            </div>

            <AmplifyAuthenticator>
                <AmplifySignUp
                    slot="sign-up"
                    formFields={[
                        { type: "username" },
                        { type: "email" },
                        {
                            type: "password",
                            label: "Password",
                            placeholder: "Enter a password"
                        },
                    ]}
                />
            </AmplifyAuthenticator>
        </div>
    )
}


export default HomeAdmin