import { Link } from "react-router-dom";

const Home = () => (
    <div>
        <h2>Home</h2>
        <Link to="/admin">Admin</Link>
    </div>
);

export default Home;