import { I18n } from "aws-amplify";
import { useLocale } from "../../hooks/useLocale";

const { default: SignUpForm } = require("../../components/SignUpForm");

function RegisterPage(props) {
    useLocale();
    return <div className="row">
        <div className="col-md-6 ml-auto mr-auto">
        <h1 className="text-muted mb-4 mt-3">{I18n.get('signup-title')}</h1>
            <SignUpForm />
        </div>
    </div>
}

export default RegisterPage;