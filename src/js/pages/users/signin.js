import SignIn from "../../components/SignIn";
import { useLocale } from "../../hooks/useLocale";

const { I18n } = require("aws-amplify");

function SignInPage(props) {
    useLocale();
    return <div className="row">
        <div className="col-md-6 ml-auto mr-auto">
        <h1 className="text-muted mb-4 mt-3">{I18n.get('signin-title')}</h1>

            <SignIn />

        </div>
    </div>
}

export default SignInPage;