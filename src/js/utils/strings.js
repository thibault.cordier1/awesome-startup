

function toSnakeCase(inputString)  {
    return inputString.split('').map((character, index) => {
        if (character === character.toUpperCase()) {
            if (index === 0) {
                return character.toLowerCase();
            } else {
                return '-' + character.toLowerCase();
            }
        } else {
            return character;
        }
    })
        .join('');
}

export {toSnakeCase};